# Portfolio projects

[Database design](https://gitlab.com/klokocnik.filip/portfolio-projects/-/tree/Database-design?ref_type=heads): Complete design and implementation of database for luxury car rental agency. 

[Property management RestAPI](https://gitlab.com/klokocnik.filip/portfolio-projects/-/tree/Property-Management-RestAPI?ref_type=heads): Property management RestAPI connected to PostgreSQL database(both run in docker). Has openAPI documentation.

[Central Parking System](https://gitlab.com/klokocnik.filip/portfolio-projects/-/tree/Central-Parking-System): Central Parking System RestAPI connected to PostgreSQL(both run in docker) I developed as a trainee SpringBoot Developer.
